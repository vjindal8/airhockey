package airHockey;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


/* ========================================
 * Authors: Trevor Leong and Varun Jindal
 * File Name: Paddle.java
 * ========================================
 * 
 * Object that houses logic for each 
 * player's paddle
 * Contains accessor methods for GameBoard
 * 
 */

public class Paddle
{
    private double currentX;
    private double currentY;
    private int score;
    private int dx;
    private int dy;
    private int playerNum;
    private static final int WIDTH = 200;
    private static final int HEIGHT = 110;
    private final int SCREEN_WIDTH;
    private final int SCREEN_HEIGHT;
    
    // Initialize Paddles on their appropriate sides. 
    // Allows for flexibility as we only need one constructor for both paddles.
    public Paddle(int playerNum, int screen_width, int screen_height) {
        score = 0;
        SCREEN_WIDTH = screen_width;
        SCREEN_HEIGHT = screen_height;
        this.playerNum = playerNum;
        if(playerNum == 1) {
            currentX = (SCREEN_WIDTH / 10) * 9;
        } else {
            currentX = (SCREEN_WIDTH / 10);
        }
        currentY = SCREEN_HEIGHT / 2;
    }
    // Collision logic included in movement to reduce clutter in GameBoard
    public void move() {
        if(playerNum == 1 && (currentX > (SCREEN_WIDTH / 2) || dx > 0)) {
            currentX += dx;
        }
        if(playerNum == 2 && (currentX < SCREEN_WIDTH / 2 || dx < 0)) {
            currentX += dx;
        }
        currentY += dy;
    }
    
    // Allow us to set movements in GameBoard.
    public void setXVel(int dx) {
        this.dx = dx;
    }
    public void setYVel(int dy) {
        this.dy = dy;
    }

    public int[] getVel() {
        return new int[] {dx, dy};
    }

    public int[] getShapePos() {
        return new int[] {0,0,0}; // TODO: fix
    }
    
    // Painting paddles here eliminates the need to access Paddle radius in GameBoard
    public void paint(Graphics g) {
        g.setColor(Color.RED);
        g.fillOval((int) currentX, (int) currentY, HEIGHT, HEIGHT);
    }

    public int[] getPos() {
        return new int[] {(int) currentX + (HEIGHT / 2), (int) currentY + (HEIGHT /2)};
    }


    //Allows us to access radius in GameBoard
    public static int getWidth() {
        return WIDTH;
    }
    
    //Allows us to access score in GameBoard
    public int getScore() {
        return score;
    }
    
    //Awards a point to the player who scored
    public void addToScore() {
        score += 1;
    }
    
    //make the paddles disappear in the game over screen
    public void end(Graphics g)
    {
        g.setColor(Color.WHITE);
        g.fillOval((int) currentX, (int) currentY, HEIGHT, HEIGHT);
    }
    
    // resets score at the end
    public void resetScore() {
        score = 0;
    }


}
