package airHockey;
import java.awt.*;

/* ========================================
 * Authors: Trevor Leong and Varun Jindal
 * File Name: Puck.java
 * ========================================
 * 
 * Object which houses logic for hockey puck
 * includes accessor methods for GameBoard
 * 
 */



public class Puck
{
    private double velX;
    private double velY;
    private double currentX;
    private double currentY;
    private final int SCREEN_WIDTH;
    private final int SCREEN_HEIGHT;
    private static final int WIDTH = 40;
    
    // initialize puck to start in the middle
    public Puck(int initVX, int initVY, int screen_width, int screen_height) {
        SCREEN_WIDTH = screen_width;
        SCREEN_HEIGHT = screen_height;
        velX = initVX;
        velY = initVY;
        currentX = 600;
        currentY = 400;
    }
    
    // Collision logic included here to keep GameBoard as organized as possible
    public void update() {
        currentX += velX;
        currentY += velY;
        // Puck will bounce if it hits a boundary
        if(currentX <20 || currentX > SCREEN_WIDTH-20) {
            velX *= -1;
        }
        if(currentY < 70 || currentY > SCREEN_HEIGHT-70) {
            velY *= -1;
        }
    }
    public void setPos(int x, int y)
    {
        currentX = x;
        currentY = y;
    }
    
    // Allows us to set velocity in GameBoard
    public void setVel(double velX, double velY) {
        this.velX = velX;
        this.velY = velY;
    }
    
    public double[] getVel() {
        return new double[] {velX, velY};
    }

    public int[] getPos() {
        return new int[] {(int) currentX + (WIDTH / 2), (int) currentY + (WIDTH / 2)};
    }
    
    // Allows us to access radius in GameBoard
    public static int getWidth() {
        return WIDTH;
    }
    
    // Allows us to paint the puck in Gameboard without having to access the radius 
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillOval((int) currentX, (int) currentY, WIDTH, WIDTH);
    }

}