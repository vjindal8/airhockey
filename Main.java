package airHockey;

import javax.swing.*;

/* =========================================
 * Authors: Trevor Leong and Varun Jindal
 * File Name: Main.java
 * =========================================
 * 
 * Main class to create JFrame
 * 
 */

public class Main
{

  public static void main(String[] args)
  {
    JFrame frame = new JFrame("AirHockey BOOSTED");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    //this object is all three listeners!
    GameBoard board = new GameBoard();

    board.addKeyListener(board);

    frame.add(board);
    frame.pack();

    frame.setSize(500, 500);
    frame.setVisible(true);

    //make sure the JPanel has the focus
    board.requestFocusInWindow();

  }

}
