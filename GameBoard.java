package airHockey;
import java.awt.event.*;
import java.util.Arrays;
import java.awt.*;
import javax.swing.*;

/* =========================================
 * Authors: Trevor Leong and Varun Jindal
 * File Name: GameBoard.java
 * =========================================  
 * 
 * Acts as ActionLister and KeyListner
 * Contains collision logic and painting logic 
 * 
 */

public class GameBoard extends JPanel implements ActionListener, KeyListener {
    public static final int SCREEN_WIDTH = 1500;
    public static final int SCREEN_HEIGHT = 800;
    public static final int BOUNDARY_HEIGHT = 50;
    public static final int GOAL_SIZE = 200;
    Paddle playerOne;
    Paddle playerTwo;
    Puck puck;
    boolean playing = false;
    boolean showTitleScreen = true;
    boolean gameOver = false;
    String winner;
    private long lastTime = System.currentTimeMillis();

    // constructor initializes two paddles, one puck, the timer, and keyListeners
    public GameBoard() {
        puck = new Puck(2, 1, SCREEN_WIDTH, SCREEN_HEIGHT);
        playerOne = new Paddle(1, SCREEN_WIDTH, SCREEN_HEIGHT);
        playerTwo = new Paddle(2, SCREEN_WIDTH, SCREEN_HEIGHT);
        setBackground(Color.WHITE);
        Timer timer = new Timer(5, this);
        timer.start();
        addKeyListener(this);
        setFocusable(true);
    }
    
    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e)
    {
      // check key releases to stop Paddle movement
        if(playing)
        {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_LEFT) {
                playerOne.setXVel(0);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN) {
                playerOne.setYVel(0);
            }
            if (e.getKeyCode() == KeyEvent.VK_W || e.getKeyCode() == KeyEvent.VK_S) {
                playerTwo.setYVel(0);
            }
            if (e.getKeyCode() == KeyEvent.VK_A || e.getKeyCode() == KeyEvent.VK_D) {
                playerTwo.setXVel(0);
            }
        }

    }

    @Override
    public void keyPressed(KeyEvent e)
    {
      // check for keyPresses for screen logic and for puck movement
        if(showTitleScreen)
        {
            if(e.getKeyCode() == KeyEvent.VK_SPACE)
            {
                showTitleScreen = false;
                playing = true;
                return;
            }
        }
        if(playing)
        {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                playerOne.setXVel(2);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                playerOne.setXVel(-2);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                playerOne.setYVel(-2);
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                playerOne.setYVel(2);
            }
            if (e.getKeyCode() == KeyEvent.VK_W) {
                playerTwo.setYVel(-2);
            } else if (e.getKeyCode() == KeyEvent.VK_S) {
                playerTwo.setYVel(2);
            }
            if (e.getKeyCode() == KeyEvent.VK_A) {
                playerTwo.setXVel(-2);
            } else if (e.getKeyCode() == KeyEvent.VK_D) {
                playerTwo.setXVel(2);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        repaint();
    }

    public void update()
    {
      // collision logic to have Puck bounce correctly
      
      boolean collided = false; // keeps track of whether puck has collided with a player
      
      int[] puckPos = puck.getPos();
      double[] puckVel = puck.getVel();
      int[] paddleOneVel = playerOne.getVel();
      int[] paddleTwoVel = playerTwo.getVel();
      int[] paddleOnePos = playerOne.getPos();
      int[] paddleTwoPos = playerTwo.getPos();
      
      double paddlePuckOneDist = Math.sqrt(Math.pow(puckPos[0] - paddleOnePos[0], 2) + Math.pow(puckPos[1] - paddleOnePos[1], 2));
      double paddlePuckTwoDist = Math.sqrt(Math.pow(puckPos[0] - paddleTwoPos[0], 2) + Math.pow(puckPos[1] - paddleTwoPos[1], 2));
  
      double normX = 0;
      double normY = 0;
      double dotProd = 0;
      if (paddlePuckOneDist <= (Puck.getWidth() + Paddle.getWidth()) / 2)
      {
        collided = true;
        // calculate the normalized vector from paddleOne to the puck
        normX = (puckPos[0] - paddleOnePos[0]) / paddlePuckOneDist;
        normY = (puckPos[1] - paddleOnePos[1]) / paddlePuckOneDist;
      }
      else if (paddlePuckTwoDist <= (Puck.getWidth() + Paddle.getWidth()) / 2)
      {
        collided = true;
        // calculate the normalized vector from paddleTwo to the puck
        normX = (puckPos[0] - paddleTwoPos[0]) / paddlePuckTwoDist;
        normY = (puckPos[1] - paddleTwoPos[1]) / paddlePuckTwoDist;
      }
      dotProd = Math.abs((puckVel[0] + puckVel[1]));
      if (dotProd == 0)
      {
        dotProd = 1.0;
      }
      double tmp_VelX = (3 * normX);
      double tmp_VelY = (3 * normY);
      if(collided && System.currentTimeMillis() - lastTime > 700) {
        // set new puck velocity as normalized vector abiding constraints
        puck.setVel(Math.min(tmp_VelX, 1.5), Math.min(tmp_VelY, 1.5));
        lastTime = System.currentTimeMillis();
      }
    }

    public void paintComponent(Graphics g)
    {
      // houses all graphics
        if (showTitleScreen)
        {
            // Display text for title screen.
            g.setColor(Color.BLACK);
            g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
            g.drawString("3D-AIR HOCKEY", 165, 100);
            g.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            g.drawString("Press the Space bar to play.", 175, 400);
        }
        else if(playing) {
            int[] puckPos = puck.getPos();
            if (puckPos[0] < 50 && Math.abs(puckPos[1] - SCREEN_HEIGHT / 2) < GOAL_SIZE / 2) {
                puck.setPos(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
                playerOne.addToScore();
                puck.setVel(0,0);
            }
            if (puckPos[0] > SCREEN_WIDTH - 50 && Math.abs(puckPos[1] - SCREEN_HEIGHT / 2) < GOAL_SIZE / 2) {
                puck.setPos(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
                playerTwo.addToScore();
                puck.setVel(0,0);
            }
            // While no player has scored 5 points, continue playing.
            if (playerOne.getScore() == 5 || playerTwo.getScore() == 5) {
                playing = false;
                gameOver = true;
            }
            super.paintComponent(g);
            g.setColor(Color.BLACK);
            //top boundary
            g.fillRect(0, 0, SCREEN_WIDTH, BOUNDARY_HEIGHT);
            //bottom boundary
            g.fillRect(0, SCREEN_HEIGHT - BOUNDARY_HEIGHT, SCREEN_WIDTH, 2 * BOUNDARY_HEIGHT);
            //First Half of left side boundary
            g.fillRect(0, 0, BOUNDARY_HEIGHT, (SCREEN_HEIGHT / 2) - GOAL_SIZE / 2);
            //Second Half of left side boundary
            g.fillRect(0, (SCREEN_HEIGHT / 2) + GOAL_SIZE / 2, BOUNDARY_HEIGHT, (SCREEN_HEIGHT) - GOAL_SIZE);
            //First half of right side boundary
            g.fillRect(SCREEN_WIDTH - BOUNDARY_HEIGHT, 0, BOUNDARY_HEIGHT * 2, (SCREEN_HEIGHT / 2) - GOAL_SIZE / 2);
            //Second half of right side boundary
            g.fillRect(SCREEN_WIDTH - BOUNDARY_HEIGHT, (SCREEN_HEIGHT / 2) + GOAL_SIZE / 2, BOUNDARY_HEIGHT * 2, (SCREEN_HEIGHT) + GOAL_SIZE);
            playerOne.paint(g);
            playerTwo.paint(g);
            playerOne.move();
            playerTwo.move();
            puck.update();
            update();
            puck.paint(g);
            g.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            g.drawString("" + playerTwo.getScore(), SCREEN_WIDTH / 8, SCREEN_HEIGHT / 8);
            g.drawString("" + playerOne.getScore(), SCREEN_WIDTH / 8*7,SCREEN_HEIGHT / 8 );
        }
        else if(gameOver)
        {
            // Display the winner and stop the game.
            if(playerOne.getScore() > playerTwo.getScore())
            {
                winner = "Player One Wins!";
            }
            else
            {
                winner = "Player Two Wins!";
            }
            g.setColor(Color.WHITE);
            g.fillRect(0,0,SCREEN_WIDTH + 300, SCREEN_HEIGHT + 300);
            g.setColor(Color.BLACK);
            g.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
            g.drawString("Game Over " + winner, 175, 400);
            g.setColor(Color.WHITE);
            playerOne.end(g);
            playerTwo.end(g);
        }
    }

}